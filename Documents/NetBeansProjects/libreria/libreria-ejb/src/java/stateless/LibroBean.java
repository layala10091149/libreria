/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Lore-PC
 */
@Stateless
public class LibroBean implements LibroBeanRemote {

   @PersistenceContext(name = "libreria-ejbPU")
    EntityManager em;
    Libro libro;
    Collection<Libro> lista_libros;

    @Override
    public void AñadeLibros(String titulo, String autor, BigDecimal precio) {
        if (libro == null) {
            libro = new Libro(titulo, autor, precio);
            em.persist(libro);
            libro = null;
        }
    }

    @Override
    public Collection<Libro> getLibro() {
        lista_libros= em.createNamedQuery("Libro.findAll").getResultList();
        return lista_libros;
    }

    @Override
    public Libro buscalibro(int id) {
        libro=em.find(Libro.class, id);
        return libro;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizarlibro(int id, String titulo, String autor, BigDecimal precio) {
        if(libro!=null){
            libro.setTitulo(titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
            libro = null;
        }//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarlibro(int id) {
        libro=em.find(Libro.class, id);
        em.remove(libro);
        libro = null;//To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
