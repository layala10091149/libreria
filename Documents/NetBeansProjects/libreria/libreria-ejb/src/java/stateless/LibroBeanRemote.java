/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author Lore-PC
 */
@Remote
public interface LibroBeanRemote {
    
    
    //parametros de los metodos que nos sirven para 
    //realizar las acciones que deseamos
    
    public void AñadeLibros(String titulo,String autor,BigDecimal precio);
    public Collection <Libro> getLibro();
    public Libro buscalibro(int id);
    public void actualizarlibro(int id, String titulo,String autor,BigDecimal precio);
    public void eliminarlibro(int id);
    
    
}
