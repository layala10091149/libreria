<%-- 
    Document   : forma
    Created on : 1/10/2014, 02:11:29 PM
    Author     : Lore-Lap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>Bienvenidos a la Libreria</h1>
        
        <form action="ClienteWeb.jsp" method="POST">
            <table>
                <tr>
                    <td>Introduzca el Titulo:</td>
                    <td><input type="text" name="t1" size="40" />
                </tr>
                
                <tr>
                    <td>Introduzca el nombre del Autor:</td>
                    <td><input type="text" name="aut" size="30" /></td>
                </tr>
                
                <tr>
                    <td>Introduzca el precio:</td>
                    <td><input type="text" name="precio" size="20" /></td>
                </tr>
                
                <tr>
                    <td><input type="submit" value="Crear" /></td>
                    <td><input type="reset" value="Limpiar" /></td>
                </tr>
            </table>
        </form>
           
    </body>
</html>
