/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package statless;


import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;
/**
 *
 * @author Lore-Lap
 */
@Remote
public interface CatalogoSessionBeanRemote {
    public void addLibro(java.lang.String titulo, java.lang.String autor, BigDecimal precio);
    public Collection <Libro> getAllLibros();

    
}
