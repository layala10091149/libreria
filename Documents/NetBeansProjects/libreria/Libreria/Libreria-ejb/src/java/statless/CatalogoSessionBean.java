/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package statless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Lore-Lap
 */
@Stateless
public class CatalogoSessionBean implements CatalogoSessionBeanRemote {
    
@PersistenceContext(unitName="Libreria-ejbPU")
EntityManager em;
protected Libro libro;
protected Collection<Libro> ListaLibros;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void addLibro(String titulo, String autor, BigDecimal precio) {
        
        if (libro==null){
        libro = new Libro(titulo,autor,precio);           
        }
        em.persist(libro);
    }

    @Override
    public Collection<Libro> getAllLibros() {
    ListaLibros=em.createNamedQuery("Libro.findAll").getResultList();
    return ListaLibros;
    }
}
